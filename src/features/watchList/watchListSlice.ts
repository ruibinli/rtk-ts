import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {AppThunk, RootState} from "../../app/store";
import {counterSlice} from "../counter/counterSlice";
import {fetchCount} from "../counter/counterAPI";

const defaultMarket = {
    tabName: 'Popular markets',
    symbols: [],
    id: '',
    sort: {
        sortId: null,
        sortOrder: null,
    },
};

interface TabInterface {
    tabName: string;
    symbols: string[],
    id: string,
    sort: {
        sortId: string | null,
        sortOrder: string | null,
    },
}

export interface WatchlistState {
    tabs: TabInterface[],
    currentTabIndex: number;
}

// 初始化状态
const initialState : WatchlistState= {
    tabs: [],
    currentTabIndex: 0,
};

export const updateAsync2 = createAsyncThunk(
    'watchList/updateAsync2',
    async (index: number) => {
        const response = await new Promise<{ data: number }>((resolve) => {
            setTimeout(() => resolve({data: index}), 500)
        })
        // The value we return becomes the `fulfilled` action payload
        return response.data;
    }
);

export const watchListSlice = createSlice({
    name: 'watchList',
    initialState,
    reducers:{
        addTab:(state, action)=>{
            state.tabs = state.tabs.concat({
                tabName:action.payload.tabName,
                symbols: [],
                id: action.payload.id,
                sort: {
                    sortId: null,
                    sortOrder: null,
                },
            });
        },
        deleteTab:(state, action)=>{
            const deleteTabIndex = state.tabs.findIndex(i=>i.id === action.payload.deleteTabId);
            state.tabs = state.tabs.splice(deleteTabIndex, 1);
        },
        editTab:(state, action)=>{
            const editTabIndex = state.tabs.findIndex(i=>i.id === action.payload.editTabId);
            state.tabs[editTabIndex].tabName = action.payload.tabName;
        },
        editSymbols:(state, action)=>{
            const editIndex = state.tabs.findIndex(o => o.id === action.payload.tabId);
            state.tabs[editIndex].symbols = action.payload.currentSymbols;
        },
        setCurrentIndex:(state, action: PayloadAction<number>) =>{
            state.currentTabIndex = action.payload;
        },
        setAccountPreferenceInWatchList:(state, action) =>{
            const {watchlist} = action.payload.preference;
            if (watchlist) {
                if(watchlist.tabs){
                    state.tabs = watchlist.tabs;
                }
            }
        },
        cleanWatchList:(state)=>initialState
    },
    extraReducers: (builder) => {
        builder.addCase(updateAsync2.fulfilled, (state, action) => {
            state.currentTabIndex = action.payload;
        }).addCase('watchList/updateAsync2/pending', (state, action) => {
            console.log(state, action);
        })
    }
});

export const selectTab = (state: RootState) => state.watchList.tabs;
export const selectCurrentTabIndex = (state: RootState) => state.watchList.currentTabIndex;

export const { setCurrentIndex, cleanWatchList } = watchListSlice.actions;

export const updateAsync = (index: number): AppThunk => (dispatch, getState) => {
    /* observable.subscribe(()=>{
      dispatch(setCurrentIndex(index))
    }) */

    setTimeout(()=>{
        dispatch(setCurrentIndex(index))
    },1000)

}

export default watchListSlice.reducer;
