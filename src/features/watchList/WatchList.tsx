import React, {FC} from 'react'
import {useAppDispatch, useAppSelector} from "../../app/hooks";
import {selectTab, selectCurrentTabIndex, setCurrentIndex, updateAsync, updateAsync2, cleanWatchList} from "./watchListSlice";

const WatchList: FC<any> = () => {
    const tabs = useAppSelector(selectTab);
    const dispatch = useAppDispatch();
    const currentTabIndex = useAppSelector(selectCurrentTabIndex);
    const change = ()=>{
        dispatch(setCurrentIndex(currentTabIndex + 1))
    }
    const changeAsync = ()=>{
        dispatch(updateAsync(currentTabIndex + 1))
    }
    const changeAsync2 = ()=>{
        dispatch(updateAsync2(currentTabIndex + 1))
    }
    const clean = ()=>{
        dispatch(cleanWatchList())
    }
    return <div>
        <div>currentTabIndex: {currentTabIndex}</div>
            <div>tabs:</div>
        <button onClick={change}>click me</button>
        <button onClick={changeAsync}>click me(async)</button>
        <button onClick={changeAsync2}>click me(async2)</button>
        <button onClick={clean}>clean</button>
            <div>
                {tabs.map(tab => <div>{tab.tabName}</div>)}
            </div>
    </div>
}


export default WatchList;
